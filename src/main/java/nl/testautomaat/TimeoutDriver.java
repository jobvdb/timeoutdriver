package nl.testautomaat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RetryMechanism {

    private static final Logger log = LoggerFactory.getLogger(RetryMechanism.class);
    /**
     * Copyright 2017 Job van den Berg
     * <p>
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     * <p>
     * http://www.apache.org/licenses/LICENSE-2.0
     * <p>
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

    private final Duration duration;
    private final Duration interval;
    private final List<Type> expectedExceptions;
    private List<Runnable> beforeRetryRunnables;
    private boolean hasFailed;
    private boolean isRetry;

    public RetryMechanism() {
        expectedExceptions = new ArrayList<>();
        beforeRetryRunnables = new ArrayList<>();
        interval = Duration.ofSeconds(1);
        duration = Duration.ofSeconds(2);
    }

    public RetryMechanism execute(Runnable runCode) {
        LocalDateTime maxTime = LocalDateTime.now().plus(duration);
        init();
        LocalDateTime elapsed = LocalDateTime.now();

        while (elapsed.isBefore(maxTime)) {
            elapsed = LocalDateTime.now();

            try {
                if (this.isRetry && beforeRetryRunnables.size() != 0) {
                    beforeRetryRunnables.forEach(beforeHook -> beforeHook.run());
                }
                runCode.run();
                clearHooks();
                return this;
            } catch (Throwable t) {
                Type theCaughtException = t.getClass();
                if (expectedExceptions.contains(theCaughtException) && elapsed.isBefore(maxTime)) {
                    log.debug(String.format("Retry code after the throwable `%s`", t.getClass().getSimpleName()));
                    log.debug("Expected Throwable thrown", t);
                    Util.safeSleep(interval.toMillis());
                    isRetry = true;
                } else {
                    hasFailed = true;
                    throw t;
                }
            }
        }
        return this;
    }

    public RetryMechanism addBeforeRetryHook(Runnable runCode) {
        this.beforeRetryRunnables.add(runCode);
        return this;
    }

    private void init() {
        if (interval.isZero() || interval.isNegative()) {
            throw new IllegalArgumentException("interval must be set to more than zero");
        }
    }

    private void clearHooks() {
        //Before hooks
        this.isRetry = false;
        this.beforeRetryRunnables.clear();

        //After hooks
        this.hasFailed = false;
    }
}
