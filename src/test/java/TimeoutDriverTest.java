import nl.testautomaat.TimeoutDriver;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JobvandenBerg on 19-6-2017.
 */
public class TimeoutDriverTest {
    TimeoutDriver timeoutDriver;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public TimeoutDriverTest() {
        timeoutDriver = new TimeoutDriver();
        timeoutDriver.Duration = Duration.ofSeconds(2);
        timeoutDriver.Interval = Duration.ofMillis(1000);
    }

    @Test
    public void canCreateTimeoutDriver() {
        TimeoutDriver timeoutDriver = new TimeoutDriver();
        timeoutDriver = new TimeoutDriver();
    }


    @Test
    public void canHandleExpectedExceptions() throws Exception {
        timeoutDriver.ExpectedExceptions.add(IndexOutOfBoundsException.class);
        timeoutDriver.Duration = Duration.ofSeconds(2);
        timeoutDriver.Interval = Duration.ofSeconds(1);
        try {
            timeoutDriver.Execute(() -> {
                List<String> list = new ArrayList<>();
                list.get(0);
            });
        } catch (Exception e) {
            Assert.assertSame(IndexOutOfBoundsException.class, e.getClass());
        }
    }

    @Test
    public void stopsAfterFirstSuccess() throws Throwable {
        String expectedReturn = "Hello World";
        LocalDateTime maxTime = LocalDateTime.now().plus(Duration.ofSeconds(5));

        timeoutDriver.Execute(() -> {
            final String returnedValue;
            List<String> list = new ArrayList<>();
            list.add(expectedReturn);
            returnedValue = list.get(0);
            Assert.assertSame(expectedReturn, returnedValue);
        });

        LocalDateTime stop = LocalDateTime.now();
        Assert.assertTrue(maxTime.isAfter(stop));
    }

    @Test(expected = IllegalArgumentException.class)
    public void mustHaveIntervalHigherThanZero() throws Throwable {
        timeoutDriver.Interval = Duration.ofSeconds(0);
        timeoutDriver.Execute(() -> {
            //nothing
        });
    }
}